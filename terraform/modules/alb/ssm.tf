resource "aws_ssm_parameter" "standard" {
    count = var.store_in_ssm ? 1 : 0
    name = "${var.env}-${var.app}-alb-https-listener-arn"
    type = "String"
    value = "${aws_alb_listener.https.arn}"
}
