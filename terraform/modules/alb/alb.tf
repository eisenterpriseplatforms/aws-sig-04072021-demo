
# Create a new load balancer
resource "aws_alb" "alb" {
  name            = "${var.env}-${var.app}-alb"
  internal        = false
  security_groups = ["${var.alb_sg}"]

  # var.vpc["subnet_ids_public"] was stringified to be used as an output, so we must split it
  subnets = "${var.public_subnet_ids}"

  # Logs go to S3://bucket/AWSLogs/aws-account-id/elasticloadbalancing/
  access_logs {
    bucket  = "${var.alb_log_bucket}"
    enabled = false
    prefix = "${var.env}-${var.app}-alb-logs"
  }

  tags = {
    Environment = "${var.env}"
    CreatedBy   = "terraform"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "redirect"

    redirect {
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "https" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "443"
  protocol          = "HTTPS"

  # ssl_policy configuration:
  # https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html#describe-ssl-policies
  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = "${var.cert}"
  default_action {
    target_group_arn = "${aws_alb_target_group.app.id}"
    type             = "forward"
  }
}


# target group for host-based routing to point to
resource "aws_alb_target_group" "app" {
  port     = 8080
  protocol = "HTTP"
  target_type = "ip"
  stickiness {
    type    = "lb_cookie"
    enabled = false
  }
  vpc_id = "${var.vpc_id}"

  health_check {
    path                = "${var.health_check_path}"
    interval            = "${var.health_check_interval}"
    timeout             = "${var.health_check_timeout}"
    healthy_threshold   = "2"
    unhealthy_threshold = "5"
  }

  tags = {
    VPC           = "${var.vpc_name}"
    Name          = "${var.env}-${var.app}"
    Environment   = var.env
    ApplicationID = var.app
    CreatedBy     = "terraform"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# data "aws_elb_service_account" "main" {}
#
# data "aws_iam_policy_document" "alb" {
#   policy_id = "alb-access"
#
#   statement = {
#     actions = ["s3:PutObject"]
#     resources = ["arn:aws:s3:::${var.alb_log_bucket}/logs/*"]
#
#     principals = {
#         identifiers = ["${data.aws_elb_service_account.main.arn}"]
#         type = "AWS"
#     }
#   }
# }
