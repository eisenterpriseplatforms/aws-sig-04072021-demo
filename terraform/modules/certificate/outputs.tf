output "info" {
	value = {
		cert_arn = aws_acm_certificate.cert.arn
   		cert_domain = aws_acm_certificate.cert.domain_name
		cert_validation_id = aws_acm_certificate_validation.cert.id
	}
}
