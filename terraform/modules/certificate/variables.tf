variable "env" {
}

variable "site_name" {
}

variable "dns_domain" {
  default = null
}

variable "custom_domain" {
  default = false
}

data "aws_route53_zone" "dns_domain" {
  count = var.dns_domain == null ? 0 : 1
  name = var.dns_domain
}

