resource "aws_acm_certificate" "cert" {
  domain_name       = "${var.site_name}.${var.dns_domain}"
  validation_method = "DNS"

  tags = {
    Environment = var.env
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn = aws_acm_certificate.cert.arn

  validation_record_fqdns = [for record in aws_route53_record.cert_validation : record.fqdn]
  depends_on = [ aws_route53_record.cert_validation]
  timeouts {
    create = "15m"
  }
}

resource "aws_route53_record" "cert_validation" {
  for_each = { 
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name = dvo.resource_record_name
      record = dvo.resource_record_value
      type = dvo.resource_record_type
    }
  }
  name = each.value.name
  records = [each.value.record]
  zone_id = data.aws_route53_zone.dns_domain.zone_id
  ttl     = 60
  type = each.value.type
  allow_overwrite = true
  depends_on = [
    aws_acm_certificate.cert
  ]
}
