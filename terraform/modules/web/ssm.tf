resource "aws_ssm_parameter" "web_bucket" {
  name  = "${var.env}-${var.site_name}-web_bucket"
  type  = "String"
  value = aws_s3_bucket.www.id
  tags = {
    STAGE     = var.env
    CreatedBy = "Terraform"
  }
}

resource "aws_ssm_parameter" "web_api_gateway" {
  name  = "${var.env}-${var.site_name}-web_api_gateway"
  type  = "String"
  value = aws_api_gateway_rest_api.standard.id
  tags = {
    STAGE     = var.env
    CreatedBy = "Terraform"
  }
}

resource "aws_ssm_parameter" "web_api_gateway_root" {
  name  = "${var.env}-${var.site_name}-web_api_gateway_root"
  type  = "String"
  value = aws_api_gateway_rest_api.standard.root_resource_id
  tags = {
    STAGE     = var.env
    CreatedBy = "Terraform"
  }
}

