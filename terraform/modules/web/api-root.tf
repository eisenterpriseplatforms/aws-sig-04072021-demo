# # Create API gateway


resource "aws_api_gateway_method" "root" {
  #depends_on  = [aws_api_gateway_deployment.standard]
  rest_api_id   = aws_api_gateway_rest_api.standard.id
  resource_id   = aws_api_gateway_rest_api.standard.root_resource_id
  http_method   = "GET"
  authorization = "NONE"
  request_parameters = {
    "method.request.querystring.idToken" = false
  }

}

#
#
resource "aws_api_gateway_integration" "root-index" {
  rest_api_id             = aws_api_gateway_rest_api.standard.id
  resource_id             = aws_api_gateway_rest_api.standard.root_resource_id
  http_method             = aws_api_gateway_method.root.http_method
  type                    = "MOCK"
  timeout_milliseconds    = 29000
  lifecycle {
    create_before_destroy = true
  }
}

#
#
resource "aws_api_gateway_method_response" "response302root" {
  rest_api_id = aws_api_gateway_rest_api.standard.id
  resource_id = aws_api_gateway_rest_api.standard.root_resource_id
  http_method = aws_api_gateway_method.root.http_method
  status_code = "302"

  #
  response_parameters = {
    "method.response.header.Location" = true
  }

  #
  response_models = {
    "application/json" = "Empty"
  }
}
