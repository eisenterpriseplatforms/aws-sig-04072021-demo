variable "env" {
}

#
# variable "s3_bucket" {}
#
# variable "s3_bucket_arn" {}
#
# variable "s3_bucket_id" {}


variable "app" {}

# variable "cert_name" {
#   type = map
#   default = null
# }

variable "custom_domain" {
  type = string
  default = false
}

variable "waf_ip_whitelist" {
  type = list(map(string))
}

variable "site_name" {
  default = "hello"
}

variable "lambda_subnets" {
}

variable "lambda_sgs" {
}

variable "dns_domain" {
  default = null
}

variable "s3_kms_arn" {
  default = null
}

data "aws_route53_zone" "dns_domain" {
  count = var.dns_domain == null ? 0 : 1
  name = var.dns_domain
}

data "aws_region" "current" {
}

data "aws_iam_policy_document" "api-fe-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "auth-lambda-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

