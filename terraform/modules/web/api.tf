# Create API gateway

resource "aws_api_gateway_rest_api" "standard" {
  name = "${var.site_name}-${var.env}-http-api"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_deployment" "standard" {
  depends_on  = [aws_api_gateway_integration.root-index]
  rest_api_id = aws_api_gateway_rest_api.standard.id
  stage_name  = var.env
}

# resource "aws_api_gateway_domain_name" "standard" {
#   count           = var.dns_domain == null ? 0 : 1
#   certificate_arn = module.cert.info["cert_arn"]
#   domain_name     = module.cert.info["cert_domain"]
# }

# resource "aws_api_gateway_base_path_mapping" "standard" {
#   count       = var.dns_domain == null ? 0 : 1
#   api_id      = aws_api_gateway_rest_api.standard.id
#   stage_name  = aws_api_gateway_deployment.standard.stage_name
#   domain_name = aws_api_gateway_domain_name.standard[0].domain_name
# }

# resource "aws_route53_record" "example" {
#   count   = var.dns_domain == null ? 0 : 1
#   name    = aws_api_gateway_domain_name.standard[0].domain_name
#   type    = "A"
#   zone_id = data.aws_route53_zone.dns_domain.0.zone_id

#   alias {
#     evaluate_target_health = true
#     name                   = aws_api_gateway_domain_name.standard[0].cloudfront_domain_name
#     zone_id                = aws_api_gateway_domain_name.standard[0].cloudfront_zone_id
#   }
# }

# module "cert" {
#   source     = "../certificate"
#   env        = var.env
#   dns_domain = var.dns_domain
#   site_name  = "demo"
# }