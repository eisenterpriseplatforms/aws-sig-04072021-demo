resource "random_string" "s3_tail" {
  length  = 5
  special = false
  upper   = false
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.env}-${var.bucket_name}-${random_string.s3_tail.result}"
  acl    = "private"
  tags = {
    Environment = var.env
    CreatedBy   = "Terraform"
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = aws_kms_key.s3_encryption.id
      }
    }
  }
}

