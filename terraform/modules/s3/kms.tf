resource "aws_kms_key" "s3_encryption" {
  description = "${var.env} KMS key for encrypting S3 bucket"
  tags = {
    Name      = "${var.env}-${var.bucket_name}-s3-encryption-key"
    CreatedBy = "Terraform"
    STAGE     = var.env
  }
}

