# Lambda roles
resource "aws_iam_role" "lambda" {
  name               = "${var.env}-${var.app}-lambda"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_1" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.vpc_lambda.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_2" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.ses_lambda.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_3" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.s3_lambda.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_4" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.kms_lambda.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_5" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.secretsmanager_lambda.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_6" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.sns_lambda.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_7" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.ddb_lambda.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_attach_8" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.stepfunction_lambda.arn
}

resource "aws_iam_user" "bitbucket" {
  name = "${var.env}-${var.app}-bitbucket-user"
  path = "/"
}

resource "aws_iam_user_policy" "bitbucket_policy" {
  name = "${var.env}-${var.app}-bitbucket-policy"
  user = aws_iam_user.bitbucket.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "StateMachinePolicy",
      "Effect": "Allow",
      "Action": [
        "states:DescribeStateMachine",
        "states:ListExecutions",
        "states:CreateActivity",
        "states:UpdateStateMachine",
        "states:DeleteStateMachine",
        "states:DeleteActivity",
        "states:CreateStateMachine"
      ],
      "Resource": [
        "arn:aws:states:*:994872636351:activity:*",
        "arn:aws:states:*:994872636351:stateMachine:*"
      ]
    },
    {
      "Sid": "ListStateMachinePolicy",
      "Effect": "Allow",
      "Action": "states:ListStateMachines",
      "Resource": "*"
    },
    {
      "Sid": "LambdaPolicy",
      "Effect": "Allow",
      "Action": "lambda:*",
      "Resource": "*"
    },
    {
      "Sid": "APIGatewayConfig",
      "Effect": "Allow",
      "Action": "apigateway:*",
      "Resource": "*"
    },
    {
      "Sid": "S3Policy",
      "Effect": "Allow",
      "Action": [
          "s3:GetEncryptionConfiguration",
          "s3:ListBucket",
          "s3:GetBucketNotification",
          "s3:GetBucketLocation",
          "s3:GetBucketPolicy"
      ],
      "Resource": "arn:aws:s3:::${var.env}-${var.app}*"
    },
    {
      "Sid": "CloudWatchLogsPolicy",
      "Effect": "Allow",
      "Action": [
          "logs:CreateLogStream",
          "logs:DescribeLogGroups",
          "logs:PutSubscriptionFilter",
          "logs:GetLogEvents",
          "logs:PutRetentionPolicy",
          "logs:DescribeQueries",
          "iam:PassRole"
      ],
      "Resource": "*"
    },
    {
      "Sid": "CloudformationPolicy",
      "Effect": "Allow",
      "Action": "cloudformation:*",
      "Resource": "*"
    },
    {
      "Sid": "SSMPolicy",
      "Effect": "Allow",
      "Action": [
          "ssm:GetParameters",
          "ssm:GetParameter"
      ],
      "Resource": "arn:aws:ssm:*:*:parameter/${var.env}*"
    },
    {
      "Sid": "AdminPolicy",
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    }
  ]
}
EOF

}

