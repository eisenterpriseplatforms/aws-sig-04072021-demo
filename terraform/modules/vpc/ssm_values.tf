resource "aws_ssm_parameter" "subnet_public" {
  count = var.store_in_ssm ? length(aws_subnet.public.*.id) : 0
  name = "${var.env}-public-subnet-id-${count.index}"
  type = "String"
  value = element(aws_subnet.public.*.id, count.index)
  tags = {
    STAGE = var.env
    CreatedBy = "Terraform"
  }
}

resource "aws_ssm_parameter" "subnet_private" {
  count = var.store_in_ssm ? length(aws_subnet.private.*.id) : 0
  name = "${var.env}-private-subnet-id-${count.index}"
  type = "String"
  value = element(aws_subnet.private.*.id, count.index)
  tags = {
    STAGE = var.env
    CreatedBy = "Terraform"
  }
}

resource "aws_ssm_parameter" "ec2_sg" {
  count = var.store_in_ssm ? 1 : 0
  name = "${var.env}-ec2-sg"
  type = "String"
  value = aws_security_group.ec2_sg.id
  tags = {
    STAGE = var.env
    CreatedBy = "Terraform"
  }
}

resource "aws_ssm_parameter" "www_sg" {
  count = var.store_in_ssm ? 1 : 0
  name = "${var.env}-www-out-sg"
  type = "String"
  value = aws_security_group.www_out_sg.id
  tags = {
    STAGE = var.env
    CreatedBy = "Terraform"
  }
}

resource "aws_ssm_parameter" "vpc_id" {
  count = var.store_in_ssm ? 1 : 0
  name = "${var.env}-vpc-id"
  type = "String"
  value = aws_security_group.www_out_sg.id
  tags = {
    STAGE = var.env
    CreatedBy = "Terraform"
  }
}
