## Subnets
# generate public/private subnet derived from var.cidr
# for each availability zone in the given region

# cidrsubnet() is confusing!  Read the supplimental documentation:
# https://ucdavis.jira.com/wiki/display/GRAD/Supplemental+Terraform+Documentation#SupplementalTerraformDocumentation-cidrsubnet()

# public
resource "aws_subnet" "public" {
  count  = var.number_of_subnets
  vpc_id = aws_vpc.standard.id
  cidr_block = cidrsubnet(
    var.cidr,
    var.subnet_public_newbits,
    count.index + length(data.aws_availability_zones.available.names),
  )
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name        = "${var.name}-${var.env} Public (${element(data.aws_availability_zones.available.names, count.index)})"
    Stage       = var.env
    Designation = "public"
    CreatedBy   = local.creator
  }
}

# private
resource "aws_subnet" "private" {
  count             = var.number_of_subnets
  vpc_id            = aws_vpc.standard.id
  cidr_block        = cidrsubnet(var.cidr, var.subnet_private_newbits, count.index)
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name        = "${var.name}-${var.env} Private (${element(data.aws_availability_zones.available.names, count.index)})"
    Stage       = var.env
    Designation = "private"
    CreatedBy   = local.creator
  }
}

resource "aws_internet_gateway" "standard" {
  vpc_id = aws_vpc.standard.id
  tags = {
    Name      = "af-${var.env}"
    CreatedBy = local.creator
    Stage     = var.env
  }
}

resource "aws_eip" "standard" {
  vpc = true
  tags = {
    Name      = "af-${var.env}"
    CreatedBy = local.creator
    Stage     = var.env
  }
}

resource "aws_nat_gateway" "standard" {
  allocation_id = aws_eip.standard.id
  subnet_id     = aws_subnet.public[0].id
  depends_on    = [aws_internet_gateway.standard]
  tags = {
    Name      = "af-${var.env}"
    CreatedBy = local.creator
    Stage     = var.env
  }
}

resource "aws_db_subnet_group" "rds" {
  name = "${var.env}-db-subnet-group"
  subnet_ids = aws_subnet.private.*.id
  tags ={
    Name = "${var.env} DB Subnet Group"
    CreatedBy = "Terraform"
    STAGE = var.env
  }
}
