module "vpc" {
  source       = "../modules/vpc"
  cidr         = var.vpc_cidr
  name         = var.app
  env          = var.env
  store_in_ssm = true
}

module "iam" {
  source        = "../modules/iam"
  app           = var.app
  env           = var.env
  s3_bucket_arn = module.s3.info["bucket_arn"]
  store_in_ssm  = true
  s3_kms_arn    = module.s3.info["bucket_kms_key"]
}

module "s3" {
  source       = "../modules/s3"
  bucket_name  = "${var.app}-static"
  env          = var.env
  store_in_ssm = true
}

module "web" {
  source           = "../modules/web"
  env              = var.env
  app              = var.app
  waf_ip_whitelist = var.waf_ip_whitelist
  site_name        = "demo"
  dns_domain       = var.domain_name
  s3_kms_arn       = module.s3.info["bucket_kms_key"]
  lambda_subnets   = module.vpc.info["subnet_ids_private"]
  lambda_sgs       = [module.vpc.info["www_out_sg"]]
  custom_domain    = false
}

