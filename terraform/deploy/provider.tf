provider "aws" {
  version = "~> 3.22.0"
  region  = "us-west-1"
}
