variable "env" {
  type = string
  default = "dev"
}

variable "app" {
}

variable "domain_name" {
}

variable "vpc_cidr" {
  type = string
  default = "10.0.0.0/16"
}

variable "waf_ip_whitelist" {
  default = [
    {
      value = "54.190.9.222/32"
      type  = "IPV4"
    },
  ]
}

# variable "ssm_vars" {
#   type = list(object({
#     name = string
#     valueFrom = string
#   }))
#   default = [
#     {
#       name = null
#       valueFrom = null
#     }
#   ]
# }
