def response_handler(status_code, message):

    response = {
        "statusCode": status_code,
        "body": message,
        "headers": {
            'Content-Type': 'text/html'
        }
    }

    return response