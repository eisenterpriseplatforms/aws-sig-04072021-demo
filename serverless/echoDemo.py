import os
import json
import utils
import boto3
import logging
from urllib.parse import unquote


log_level = os.environ['log_level'] if 'log_level' in os.environ else "DEBUG"
logger = logging.getLogger()
logger.setLevel(log_level)

def handler(event, context):
    logger.info(event)
    logger.info(event['pathParameters'])

    if (event['pathParameters'] is None):
        returnPage = "Hello world without echo!"
    else:
        arg = event['pathParameters']['proxy']
        returnPage = createPage(unquote(arg))

    return utils.response_handler(200, returnPage)




def createPage(echoText):
    page = f"""<html>
    <head></head>
    <body><p>{echoText}</p></body>
    </html>"""
    return page
