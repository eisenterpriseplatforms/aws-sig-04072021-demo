# README #

This document details how to use the code in this repository to build the "Echo" web application using Terraform, Serverless and Bitbucket Pipelines

### Pre-requisites ###
1. An AWS account and AWS IAM keys
2. Terraform version 0.13.5: https://releases.hashicorp.com/terraform/
3. Terraform installed: https://learn.hashicorp.com/tutorials/terraform/install-cli
4. Serverless installed: https://www.serverless.com/framework/docs/providers/aws/guide/installation/
5. A Bitbucket repository with Bitbucket Pipelines enabled: https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/
6. Terraform documentation for AWS provider https://registry.terraform.io/providers/hashicorp/aws/latest/docs

### Getting started with Terraform ###

1. Open a command line that will allow you to use terraform (this will vary depending on how you installed the application) 
2. Modify the "test.tfvars" file as you need to match your requirements
3. After cloning the repository navigate to the "terraform" directory
4. Run 'terraform apply --var-file=test.tfvars'

### Getting started with Serverless ###

1. Open a command line that will allow you to use serverless (this may vary depending on how you've installed the application)
2. Navigate to to the "serverless" directory
3. Run 'serverless deploy --stage=test'

### Check the status of the application ###

1. Login to the AWS console
2. Navigate to the API Gateway console
3. Click on "Stages" in the right-hand menu
4. Copy the "Invoke URL"
5. Browse to "https://[Invoke URL]/echo/" to confirm that the web application is functioning


### Integrating with Bitbucket Pipelines ###

1. Login to the AWS console
2. Navigate to the IAM console
3. Click on "Users" in the right-hand menu
4. Select the Bitbucket user (it will be the one with "bitbucket-user" somewhere in it's name)
5. Click on the "Security Credentials" tab
6. Click the "Create Access Keys" button
7. Copy the Access Key ID and Secret Access Key values
8. In your Bitbucket repository go to "Responsitory Settings"
9. In the "Pipelines" section click on "Settings"
10. Confirm that Bitbucket pipelines are enabled
11. In the left-hand menu, click on "Deployments"
12. Click the "Add Environment" button
13. Add an environment called "demo"
14. Expand the environment
15. Add a secure variable called "AWS_ACCESS_KEY_ID" with the "Access Key ID" from step 7 as the value
16. Add a secure variable called "AWS_SECRET_ACCESS_KEY" with the "Securet Access Key" from step 7 as the value
17. Add a variable called "AWS_DEFAULT_REGION" with the region where you will deploy the demo code (default is us-west-1).
18. Clone and commit the contents of this respository to the master branch of the respository you've configured in the previous steps

For questions, contact itarshis@ucdavis.edu